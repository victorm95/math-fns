function sum(a: number, b: number) {
  return a + b;
}

function add(a: number) {
  return function (b: number) {
    return a + b;
  };
}

function times(a: number) {
  return function (b: number) {
    return a * b;
  };
}

module.exports = {
  sum,
  add,
  times,
};
